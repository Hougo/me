---
title: A propos
---

# A propos de moi

{{< figure class="avatar" src="/avatar.jpg" >}}

Futur ingénieur informatique passionné par l'informatique libre et open-source.   
Je cherche maintenant à mettre mes compétences au service des communs.

## Centre d'intérêt
- Connaître tout de l'informatique libre et open-source
- Auto-héberger pour moi et mes proches
- Réfléchir et discuter de l'avenir : résilience, écologie, etc...
- Ecouter de la musique : jazz et metal
- Bricoler

## Compétences
- Architecture : logiciel
- Gestion de projet : Agile (Scrum)
- Languages de programmation : Javascript, Go, C#, Java, Python, PHP, C
- Frameworks font : Vue, React, Angular, Vanilla JS
- Frameworks back : Express JS, NestJS, ASP.NET Core, Symfony
- Base de données : PostgreSQL, MongoDB, Redis
- DevOps : GitLab CI, Drone.io, Jenkins, Ansible, Docker
- Système de fichiers : ZFS
- Système d'exploitation : ~~Windows~~, Linux

## Missions freelance
- Mise en place d'une infrastructure DevOps
- Internationalisation des interfaces et des données d'une application web grand public.
- Développement d'un logiciel libre permettant de gérer des tournées de collecte. 
- Mise en place d'une infrastructure informatique centralisé sur un serveur avec Active Directory et un dossier partagé SAMBA.
- Développement d'un logiciel en C# permettant de générer un bilan des flux et des ventes d'une ressourcerie au format Excel.

## Stages
| Durée | Entreprise | Rôle
|-------|---------|------------
| 4 mois |  Sopra Steria  (Espagne)| DevOps
| 4 mois | Sopra Steria | Développeur mobile / fullstack
| 3 mois | Sopra Steria | Développeur fullstack
| 2 mois | Bittle | Développeur fullstack

## Scolarité
| Période | Ecole | Diplôme | Spécialité |
|---------|-------|---------|------------|
| 2016 - 2021 | CESI | Ingénieur Informatique | Logiciel, Cybersécurité |
| 2013 - 2016 | Lycée Marseilleveyre | Baccalauréat | Sciences de l'ingénieur

## Langues
- Français : natale
- Anglais : professionel
- Esparento : novice